require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TemplateProject
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # set default locale
    config.i18n.default_locale= :es
    # and load locales for models and views
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'models', '*', '*.yml')]
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'views', '*', '*.yml')]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
