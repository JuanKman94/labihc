export default {
    types: [
        'match',
        'math',
        'options',
        'multiple', // multiple options
    ]
};

export const Questions = [
    {
        type: 'options',
        text: 'Los trabalenguas sirven para',
        className: 'spanish',
        answer: null,
        options: [
            'divertir',
            'conversar',
            'aprender',
            'fastidiar'
        ],
        isCorrect (answer) {
            return answer == 'divertir';
        }
    },
    {
        type: 'options',
        text: 'El siguiente texto es un poema porque:<br><br><center>Los astros son rondas de niños,<br>jugando la Tierra a espiar...<br>Los trigos son talles de niñas<br>jugando a ondular... a ondular...<br>Los ríos son rondas de niños<br>jugando a encontrarse en el mar...<br>Las olas son rondas de niñas,<br>jugando la Tierra a abrazar...</center>',
        className: 'spanish',
        answer: null,
        options: [
            'Narra cómo juegan los niños y las niñas.',
            'Los niños y las niñas hablan sobre sus juegos.',
            'Habla sobre las rondas usando palabras que riman entre sí.',
            'Describe con detalle cómo jugar a las rondas.',
        ],
        isCorrect (answer) {
            return answer == 'Habla sobre las rondas usando palabras que riman entre sí.';
        }
    },
    {
        type: 'options',
        text: 'Manuel tiene una mascota y quiere, a través de un programa de televisión, informarse sobre cómo educar bien a su perro. De los siguientes programas que vio, el que más le sirve es:',
        className: 'spanish',
        answer: null,
        options: [
            'Vida salvaje. Esta es la historia de Max Rodríguez, un extravagante e inexperto explorador que aprendió sobre la naturaleza viendo documentales.',
            'Corazón en la Tierra. En la granja de la familia Fleming, se rehabilitan animales domésticos problemáticos o traumatizados, para darles una nueva oportunidad en la vida.',
            'El líder de la manada. El protagonista viaja por el mundo para enseñarle al público las tres reglas principales que se deben seguir para adoptar o comprar un perro, con la intención de terminar con el abandono de especies caninas en el mundo.',
            'Mundo salvaje. En un mundo increíble, mundo lleno de aventuras y palabras, el perro, el pato, la oveja, el sapo y el cerdo construyen sus propias historias con palabras que se convierten en cosas. En cada episodio, los amigos intentan resolver retos gracias al poder de las palabras.',
        ],
        isCorrect (answer) {
            return answer == 'El líder de la manada. El protagonista viaja por el mundo para enseñarle al público las tres reglas principales que se deben seguir para adoptar o comprar un perro, con la intención de terminar con el abandono de especies caninas en el mundo.';
        }
    },
    {
        type: 'options',
        text: 'Marta quiere publicar un artículo en el periódico mural de su escuela.<br>Para ello, ha escrito una lista de ideas, algunas de las cuales son:<br><br>1- Hay lugares en los que el verde de la naturaleza ha desaparecido.<br>2- Es necesario crear normas internacionales que ayuden a frenar el calentamiento global.<br>3- Los gobiernos locales deben tomar decisiones que permitan que muchos materiales se puedan reciclar.<br>4- Es necesario separar la basura orgánica de la inorgánica, en la casa y en la ciudad.<br><br>Según su lista de ideas, en el artículo, Marta tiene la intención de:',
        className: 'spanish',
        answer: null,
        options: [
            'Informar sobre las consecuencias del cambio climático.',
            'Presentar propuestas para cuidar el medio ambiente.',
            'Proponer viajes a lugares sin vegetación o contaminados.',
            'Presentar una queja por el maltrato dado a la naturaleza.',
        ],
        isCorrect (answer) {
            return answer == 'Presentar propuestas para cuidar el medio ambiente.';
        }
    },
    {
        type: 'options',
        text: 'Laura participará en un concurso de poesía cuyo tema es el paisaje. Ella ha escrito algunos versos, pero hay uno que NO le parece apropiado para su poema. ¿Cuál de los siguientes versos NO es apropiado para su poema?',
        className: 'spanish',
        answer: null,
        options: [
            'Cuando amanece mi ventana es una obra de arte.',
            'y pinceladas de colores recrean la vida del paisaje.',
            'el paisaje se clasifica en natural o artificial.',
            'ahora duerme, paisaje tranquilo, con alegría mañana te volveré a ver.',
        ],
        isCorrect (answer) {
            return answer == 'el paisaje se clasifica en natural o artificial.';
        }
    },
    /*
    {
        type: 'options',
        text: 'El profesor le pide a Lula que prepare una exposición sobre la elaboración de la miel, ya que su mamá la recolecta y la vende en el pueblo. La acción que menos le sirve a Lula para preparar su exposición es:',
        className: 'spanish',
        answer: null,
        options: [
            'Consultar en la enciclopedia los pasos para la elaboración de la miel.',
            'Hablar con su mamá sobre la recolección de la miel.',
            'Ir hasta donde están los panales y observar a las abejas.',
            'Hablar con las personas que venden la miel en sus tiendas.',
        ],
        isCorrect (answer) {
            return answer == 'Hablar con las personas que venden la miel en sus tiendas.';
        }
    },
    {
        type: 'options',
        text: 'El colegio Simón Bolívar está cumpliendo 20 años y las directivas van a hacer un gran evento en la caseta comunal para celebrarlo con toda la comunidad. Para esto, se ha invitado a todos los estudiantes a crear un cartel que informe sobre el evento. Arturo y Luisa, quienes son estudiantes de grado 4º, quieren participar para ganar el premio que ofrecen al mejor grupo (un paseo).<br><br>¿Qué información deben tener en cuenta los estudiantes para incluir en el cartel?',
        className: 'spanish',
        answer: null,
        options: [
            'Qué se va a celebrar, cuándo, dónde, quiénes pueden ir y a qué hora.',
            'El lugar donde se va a realizar el evento y el motivo de la celebración.',
            'Que el colegio va a cumplir 20 años y sus directivas quieren celebrarlo con toda la comunidad.',
            'Qué se van a ganar, quiénes pueden hacer el afiche y para qué es.',
        ],
        isCorrect (answer) {
            return answer == 'Qué se va a celebrar, cuándo, dónde, quiénes pueden ir y a qué hora.';
        }
    },
    {
        type: 'options',
        text: 'La palabra “chistoso”, es sinónimo de…',
        className: 'spanish',
        answer: null,
        options: [
            'soso',
            'aburrido',
            'serio',
            'alegre',
        ],
        isCorrect (answer) {
            return answer == 'alegre';
        }
    },
    {
        type: 'options',
        text: 'Completa lo siguiente: “La comida es al cuerpo como la gasolina es al…',
        className: 'spanish',
        answer: null,
        options: [
            'automóvil',
            'bicicleta',
            'teclado',
            'escritorio',
        ],
        isCorrect (answer) {
            return answer == 'automóvil';
        }
    },
    {
        type: 'options',
        text: 'A Pablito le dejaron de tarea investigar con sus vecinos acerca de lo que opinan de la recolección de basura y decidió entrevistarlos. Los signos que más utilizó al escribir las preguntas son:',
        className: 'spanish',
        answer: null,
        options: [
            'las comillas',
            'signos de interrogación',
            'signos de exclamación',
            'las llaves',
        ],
        isCorrect (answer) {
            return answer == 'signos de interrogación';
        }
    },
    {
        type: 'options',
        text: '¿Cómo se escribirá la siguiente palabra si la cambias a plural?<br><b>Lombriz</b>',
        className: 'spanish',
        answer: null,
        options: [
            'lombrises',
            'lombrizes',
            'lombrices',
            'lombriceses',
        ],
        isCorrect (answer) {
            return answer == 'lombrices';
        }
    },
    */
    {
        type: 'math',
        text: 'Mario compró 1 kilogramo de manzana de $23.50, 2 piezas de lechuga de $0.85 y 1 kilogramo de plátano de $23.50. ¿Cuánto pagó en total?',
        className: 'math',
        answer: null,
        isCorrect (answer) {
            answer = parseFloat(answer.replace(/[^0-9\.]/, ''));
            answer = parseFloat(Math.round(answer * 100) / 100).toFixed(2);
            return 48.90 == answer;
        }
    },
    {
        type: 'options',
        text: 'Si tuvieras que descomponer $3,250 en la menor cantidad de billetes, ¿cómo lo harías?',
        className: 'math',
        answer: null,
        options: [
            '6 billetes de $500, 2 de $100 y 1 de $50',
            '5 billetes de $500, 2 de $200 y 7 de $50',
            '16 billetes de $200 y 1 de $50',
            '6 billetes de $500, y 5 de $50',
        ],
        isCorrect (answer) {
            return answer == '6 billetes de $500, 2 de $100 y 1 de $50';
        }
    },
    {
        type: 'options',
        text: 'En un estacionamiento hay 187 carros, si cada carro tiene 4 llantas, ¿Cuántas llantas hay en total?',
        className: 'math',
        answer: null,
        options: [
            '748 llantas',
            '648 llantas',
            '820 llantas',
            '428 llantas',
        ],
        isCorrect (answer) {
            return answer == '748 llantas';
        }
    },
    {
        type: 'options',
        text: 'Juan tenía $250 pesos y compró unos zapatos que le costaron $70 pesos. Después, compró tres camisas al mismo precio. Ahora le quedan $15 pesos. ¿Cuánto costaba cada camisa?',
        className: 'math',
        answer: null,
        options: [
            '162 pesos',
            '45 pesos',
            '65 pesos',
            '55 pesos',
        ],
        isCorrect (answer) {
            return answer == '55 pesos';
        }
    },
    {
        type: 'options',
        text: 'El alimento preferido del lagarto son las hormigas. Puede comer hasta 45 hormigas por minuto. ¿Cuánto tiempo tarda en comer 540 hormigas?',
        className: 'math',
        answer: null,
        options: [
            '10 minutos',
            '12 minutos',
            '9 minutos',
            '8 minutos',
        ],
        isCorrect (answer) {
            return answer == '9 minutos';
        }
    },
    /*
    {
        type: 'options',
        text: 'Boris compró 957 camisas para vender en 3 almacenes, ¿Cuántas camisas debe enviar a cada almacén si quiere que cada uno tenga la misma cantidad?',
        className: 'math',
        answer: null,
        options: [
            '300 camisas',
            '319 camisas',
            '960 camisas',
            '400 camisas',
        ],
        isCorrect (answer) {
            return answer == '319 camisas';
        }
    },
    {
        type: 'options',
        text: 'Completa el número que falta (____ + 53 = 100)',
        className: 'math',
        answer: null,
        options: [
            '57',
            '25',
            '37',
            '47',
        ],
        isCorrect (answer) {
            return answer == '47';
        }
    },
    {
        type: 'options',
        text: '¿Cuánto es 3400 + 2000?',
        className: 'math',
        answer: null,
        options: [
            '5400',
            '7500',
            '3400',
            '4700',
        ],
        isCorrect (answer) {
            return answer == '5400';
        }
    },
    {
        type: 'options',
        text: '¿Cuánto es 5992 - 700?',
        className: 'math',
        answer: null,
        options: [
            '5482',
            '5292',
            '4092',
            '5002',
        ],
        isCorrect (answer) {
            return answer == '5292';
        }
    },
    {
        type: 'options',
        text: 'En un zoológico hay 246 aves de diferente tipo, si cuento cada una de sus patas. ¿Cuántas patas habré contado?',
        className: 'math',
        answer: null,
        options: [
            '444 patas',
            '348 patas',
            '246 patas',
            '492 patas',
        ],
        isCorrect (answer) {
            return answer == '492 patas';
        }
    },
    {
        type: 'options',
        text: 'El papá de Adriana le entrega a su mamá 3 billetes de $200, 2 de $100, 1 de $50 y 3 de $20, ¿cuánto dinero recibe en total?',
        className: 'math',
        answer: null,
        options: [
            '$880',
            '$870',
            '$92',
            '$910',
        ],
        isCorrect (answer) {
            return answer == '$910';
        }
    },
    {
        type: 'options',
        text: 'Una señora compró 8 paquetes con seis jugos cada uno, para llevar a una fiesta, ¿Cuántos jugos llevará a la fiesta?',
        className: 'math',
        answer: null,
        options: [
            '37',
            '60',
            '48',
            '42',
        ],
        isCorrect (answer) {
            return answer == '48';
        }
    },
    {
        type: 'options',
        text: 'A mí me toca sacar la basura los martes, jueves y sábados; mi papá me da 7 pesos cada semana por ese trabajo. Si ahorro lo que me da, ¿cuánto juntaré al paso de 20 semanas?',
        className: 'math',
        answer: null,
        options: [
            '100 pesos',
            '135 pesos',
            '89 pesos',
            '140 pesos',
        ],
        isCorrect (answer) {
            return answer == '140 pesos';
        }
    },
    */
];
