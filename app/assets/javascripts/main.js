import Vue from 'vue';
import { Questions } from './questions.js';
import MatchPairs from 'vue-match-pairs';

Vue.use(MatchPairs);

let images = [
    '/adventure-time.png',
    '/frozen-elsa.jpg',
    '/jack-sparrow.jpg',
    '/spiderman.jpg',
    '/spongebob.png'
];

new Vue({
    el: '#ihc',
    components: {
    },
    data() {
        return {
            questions: Questions,
            score: 0,
            matchImages: images,
        };
    },

    methods: {
        check(ev) {
            this.score = 0;

            let q,
                perc = 100 / this.questions.length;

            for (var i in this.questions) {
                q = this.questions[i];
                if (q.answer == null) continue;

                if (q.isCorrect(q.answer)) {
                    this.score += perc;
                }
            }

            this.score = this.score.toFixed(2);
        },

        setQuestionAnswer(q, answer) {
            q.answer = answer;
            this.check();
        }
    },

    computed: {
        mathQuestions() {
            return this.questions.filter( (el, i, arr) => {
                return el.className == 'math';
            });
        },

        spanishQuestions() {
            return this.questions.filter( (el, i, arr) => {
                return el.className == 'spanish';
            });
        },
    },
})
