# Lab IHC

School project for Human-Computer Interaction class. 'Tis to be an educational
application for kids with hyperactivity disorder.

## Styles

We're following the material guidelines with the marvelous
[materialize](https://materializecss.com) framework.

## Heroku

To configure deploy using a NPM asset pipeline see
[here](https://stackoverflow.com/questions/18694887/rails-heroku-how-to-install-javascript-dependences-that-needs-npm-install)

---

This project is created from a GitLab _project template_.
See [the original project](https://gitlab.com/gitlab-org/project-templates/rails)
