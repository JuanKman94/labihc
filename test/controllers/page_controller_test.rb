require 'test_helper'

class PageControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "should redirect to login" do
    get page_index_url
    assert_response :redirect
  end

  test "it responds with authentication" do
    sign_in users(:one)
    get '/'
    assert_response :ok
  end
end
